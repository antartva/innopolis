import asyncio
import aiohttp
import re, datetime
openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'

class City_Weather():
    def __init__(self,city_name,city_temp,city_humidity,city_pressure):
        self.city_name = city_name
        self.city_temp = city_temp
        self.city_humidity = city_humidity
        self.city_pressure = city_pressure

def weater_pars(r_text):
    city_name = re.search(r'name":"[a-zA-Z -]+', r_text).group(0).split(":")[1].replace('"', "")
    try:
        city_temp = re.search(r'temp":\d+.\d+', r_text).group(0).split(":")[1]
    except:
        city_temp = re.search(r'temp":-\d+.\d+', r_text).group(0).split(":")[1]
    city_humidity = re.search(r'humidity":\d+', r_text).group(0).split(":")[1]
    city_pressure = re.search(r'pressure":\d+', r_text).group(0).split(":")[1]
    current_city_weather = City_Weather(city_name, city_temp, city_humidity, city_pressure)
    return current_city_weather

async def get_page_data(city_name):
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        url = f"https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={openweatherAPI}&units=metric"
        async with session.get(url=url) as response:
                if response.status == 200:
                    print("*" * 30)
                    current_city_weather = weater_pars(await response.text())
                    print(f'Город:\t{current_city_weather.city_name}\nТемпература: {current_city_weather.city_temp}C°\n' \
                          f'Влажность: {current_city_weather.city_humidity}%\n'
                          f'Давление: {current_city_weather.city_pressure}мм р.т.')
start_time = datetime.datetime.now()
def get_cities():
    cities = []
    with open('cities.txt','r') as file:
        lines = file.readlines()
        for line in lines:
            city = re.findall(r'[A-z]+', line)
            if len(city) == 1:
               cities.append(city[0])
            elif len(city) > 1:
                cities.append(city[0]+' '+city[1])
    return cities
cities = get_cities()
print(cities,type(cities))

async def main():
    tasks = []
    for city in cities:
        print(city)
        task = asyncio.create_task(get_page_data(city))
        tasks.append(task)
    await asyncio.gather(*tasks)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
